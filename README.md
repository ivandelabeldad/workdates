# PHP WorkDates

[![Packagist](https://img.shields.io/packagist/v/ivandelabeldad/workdates.svg)](https://packagist.org/packages/ivandelabeldad/workdates)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Workdates library for PHP

## Usage

### Get total hours and earnings from multiple dates
```php
// CREATE THE LIST
$list = new WorkDateArrayList();

// ADD ELEMENTS TO THE LIST AND HOURS PER DAY / HOUR WAGES
$list->addAll([
    WorkDate::now(10, 5),
    WorkDate::now(10, 5),
]);

// WORKED HOURS
$hours = $list->workedHours();

// TOTAL EARNINGS
$earnings = $list->earnings();
```

## License

The API Rackian is open-sourced software licensed under
the [MIT LICENSE](https://opensource.org/licenses/MIT)
