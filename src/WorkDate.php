<?php

namespace WorkDate;

use Date\Date;
use Date\Month;

class WorkDate extends Date
{

    /** @var float */
    protected $hoursOfWorkPerDay;
    /** @var float */
    protected $hourlyWage;

    /**
     * @return float
     */
    public function getHoursOfWorkPerDay()
    {
        return floatval($this->hoursOfWorkPerDay);
    }

    /**
     * @param float $hoursOfWorkPerDay
     */
    public function setHoursOfWorkPerDay($hoursOfWorkPerDay)
    {
        $this->hoursOfWorkPerDay = $hoursOfWorkPerDay;
    }

    /**
     * @return float
     */
    public function getHourlyWage()
    {
        return floatval($this->hourlyWage);
    }

    /**
     * @param float $hourlyWage
     */
    public function setHourlyWage($hourlyWage)
    {
        $this->hourlyWage = $hourlyWage;
    }

    /**
     * @return float
     */
    public function earnings()
    {
        return floatval($this->hourlyWage * $this->hoursOfWorkPerDay);
    }

    /**
     * @param int $year
     * @param int $month
     * @param int $day
     * @param float $hoursOfWorkPerDay
     * @param float $hourlyWage
     * @return WorkDate
     */
    public static function create(
        $year = 1970,
        $month = Month::JANUARY,
        $day = 1,
        $hoursOfWorkPerDay = 0.0,
        $hourlyWage = 0.0)
    {
        $date = new WorkDate();
        $time = mktime(0, 0, 0, $month, $day, $year);
        $date->setUnixTime($time);
        $date->setHoursOfWorkPerDay($hoursOfWorkPerDay);
        $date->setHourlyWage($hourlyWage);
        return $date;
    }

    /**
     * @param float $hoursOfWorkPerDay
     * @param float $hourlyWage
     * @return WorkDate
     */
    public static function now($hoursOfWorkPerDay = 0.0, $hourlyWage = 0.0)
    {
        $date = new WorkDate();
        $date->setUnixTime(time());
        $date->setHoursOfWorkPerDay($hoursOfWorkPerDay);
        $date->setHourlyWage($hourlyWage);
        return $date;
    }

    /**
     * @param int $time
     * @param float $hoursOfWorkPerDay
     * @param float $hourlyWage
     * @return WorkDate
     */
    public static function fromUnixTime($time = 0, $hoursOfWorkPerDay = 0.0, $hourlyWage = 0.0)
    {
        $date = new WorkDate();
        $date->setUnixTime($time);
        $date->setHoursOfWorkPerDay($hoursOfWorkPerDay);
        $date->setHourlyWage($hourlyWage);
        return $date;
    }

}
