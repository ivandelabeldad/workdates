<?php

namespace WorkDate;

class WorkDateFilter
{

    /** @var WorkDateArrayList */
    protected $workDateArrayList;

    /**
     * WorkDateFilter constructor.
     * @param WorkDateArrayList $workDateArrayList
     */
    protected function __construct($workDateArrayList)
    {
        $this->workDateArrayList = $workDateArrayList;
    }

    /**
     * @param WorkDateArrayList $workDateArrayList
     * @return WorkDateFilter
     */
    public static function builder($workDateArrayList)
    {
        return new WorkDateFilter($workDateArrayList);
    }

    /**
     * @return WorkDateArrayList
     */
    public function build()
    {
        return $this->workDateArrayList;
    }

    /**
     * @param int[] $years
     * @return WorkDateFilter
     */
    public function filterByYears(array $years)
    {
        $this->workDateArrayList = $this->workDateArrayList->filter(function (WorkDate $date) use ($years) {
            return in_array($date->getYear(), $years);
        });
        return $this;
    }

    /**
     * @param int[] $month
     * @return WorkDateFilter
     */
    public function filterByMonths(array $month)
    {
        $this->workDateArrayList = $this->workDateArrayList->filter(function (WorkDate $date) use ($month) {
            return in_array($date->getMonth()->getNumber(), $month);
        });
        return $this;
    }

    /**
     * @param int[] $days
     * @return WorkDateFilter
     */
    public function filterByDays(array $days)
    {
        $this->workDateArrayList = $this->workDateArrayList->filter(function (WorkDate $date) use ($days) {
            return in_array($date->getDay(), $days);
        });
        return $this;
    }

    /**
     * @param int[] $days
     * @return WorkDateFilter
     */
    public function filterByDaysOfWeek(array $days)
    {
        $this->workDateArrayList = $this->workDateArrayList->filter(function (WorkDate $date) use ($days) {
            return in_array($date->getDayOfWeek()->getNumber(), $days);
        });
        return $this;
    }

    /**
     * @param int[] $seasons
     * @return WorkDateFilter
     */
    public function filterBySeasons(array $seasons)
    {
        $this->workDateArrayList = $this->workDateArrayList->filter(function (WorkDate $date) use ($seasons) {
            return in_array($date->getSeason()->getNumber(), $seasons);
        });
        return $this;
    }

}
