<?php

namespace WorkDate;

class WorkDateUtils
{

    /**
     * @param WorkDate $start
     * @param WorkDate $end
     * @return WorkDateArrayList
     * @internal param $date
     */
    public static function datesBetween(WorkDate $start, WorkDate $end)
    {
        if ($start->getUnixTime() > $end->getUnixTime()) {
            $minDate = $end;
            $maxDate = $start;
        } else {
            $minDate = $start;
            $maxDate = $end;
        }
        $dates = [];
        while ($minDate->getUnixTime() <= $maxDate->getUnixTime()) {
            array_push($dates, $minDate);
            $minDate = WorkDate::create($minDate->getYear(), $minDate->getMonth()->getNumber(), $minDate->getDay() + 1);
        }
        return new WorkDateArrayList($dates);
    }

}
