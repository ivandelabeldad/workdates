<?php

namespace WorkDate;

use Collection\ArrayList;
use Date\Date;
use Date\DateArrayList;
use Date\DateComparator;

class WorkDateArrayList extends DateArrayList
{

    /** @var WorkDate */
    protected $type;

    /**
     * WorkDateArrayList constructor.
     * @param WorkDate[] $elements
     */
    public function __construct($elements = [])
    {
        parent::__construct($elements);
        $this->type = WorkDate::now();
    }

    /**
     * @param WorkDate $element
     */
    public function add($element)
    {
        parent::add($element);
    }

    /**
     * @param int $index
     * @param WorkDate $element
     */
    public function addAt($index, $element)
    {
        parent::addAt($index, $element);
    }

    /**
     * @param WorkDate[] $elements
     */
    public function addAll($elements)
    {
        parent::addAll($elements);
    }

    /**
     * @param int $index
     * @param WorkDate[] $elements
     */
    public function addAllAt($index, $elements)
    {
        parent::addAllAt($index, $elements);
    }

    /**
     * @param int $index
     * @return WorkDate|Date
     */
    public function get($index)
    {
        return parent::get($index);
    }

    /**
     * @param int $index
     */
    public function removeAt($index)
    {
        parent::removeAt($index);
    }

    /**
     * @param WorkDate $element
     */
    public function remove($element)
    {
        parent::remove($element);
    }

    /**
     *
     */
    public function clear()
    {
        parent::clear();
    }

    /**
     * @return int
     */
    public function size()
    {
        return parent::size();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return parent::isEmpty();
    }

    /**
     * @return WorkDate[]|Date[]
     */
    public function toArray()
    {
        return parent::toArray();
    }

    /**
     * @param WorkDate $element
     * @return bool
     */
    public function containts($element)
    {
        return parent::containts($element);
    }

    /**
     * @param WorkDate $element
     * @return int
     */
    public function indexOf($element)
    {
        return parent::indexOf($element);
    }

    /**
     * @param callable|DateComparator $comparator
     * @param bool $ascendent
     */
    public function sort($comparator, $ascendent = true)
    {
        parent::sort($comparator, $ascendent);
    }

    /**
     * @param callable $callable
     * @return void
     */
    public function forEachDo(callable $callable)
    {
        parent::forEachDo($callable);
    }

    /**
     * @param callable $callable
     * @return ArrayList
     */
    public function map(callable $callable)
    {
        return parent::map($callable);
    }

    /**
     * @param callable $callable
     * @return WorkDateArrayList
     */
    public function filter(callable $callable)
    {
        $dates = parent::filter($callable);
        return new WorkDateArrayList($dates->toArray());
    }

    /**
     * @param callable $callable
     * @param mixed $initialValue
     * @return mixed
     */
    public function reduce(callable $callable, $initialValue)
    {
        return parent::reduce($callable, $initialValue);
    }

    /**
     * @return float
     */
    public function workedHours()
    {
        return $this->map(function (WorkDate $workDate) {
            return $workDate->getHoursOfWorkPerDay();
        })->reduce(function ($hoursPerDay1, $hoursPerDay2) {
            return $hoursPerDay1 + $hoursPerDay2;
        }, 0);
    }

    /**
     * @return float
     */
    public function earnings()
    {
        return $this->map(function (WorkDate $workDate) {
            return $workDate->earnings();
        })->reduce(function ($earnings1, $earnings2) {
            return $earnings1 + $earnings2;
        }, 0);
    }

}
